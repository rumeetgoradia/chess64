package chess;

/**
 * Representation for a knight in chess.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */
public class Knight extends Piece {

	/**
	 * Initializes a Knight chess Piece using type 'N'.
	 *
	 * @param team
	 *            the team to which this Knight belongs
	 */
	public Knight(char team) {
		super(team, 'N');
	}

	/**
	 * Checks the legality of the proposed move for this Knight. In-between
	 * collisions are irrelevant for this type of Piece, so the only check required
	 * is whether this Knight moved appropriately.
	 *
	 * @see chess.Piece#isMoveLegal(chess.Square, chess.Square, chess.Board)
	 */
	@Override
	public boolean isMoveLegal(Square oldPos, Square newPos, Board board) {
		if (!super.isMoveLegal(oldPos, newPos, board)) {
			return false;
		}
		int x = Math.abs(oldPos.getX() - newPos.getX());
		int y = Math.abs(oldPos.getY() - newPos.getY());
		return (x * y == 2);
	}

}
