package chess;

/**
 * Representation of a bishop in chess.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */
public class Bishop extends Piece {

	/**
	 * Initializes a Bishop chess Piece using type 'B'.
	 *
	 * @param team
	 *            the team to which this Piece belongs
	 */
	public Bishop(char team) {
		super(team, 'B');
	}

	/**
	 * Checks the legality of the proposed move for this Bishop. This Piece can only
	 * move diagonally, so the difference in its horizontal movement must equal the
	 * difference in its vertical movement. In addition, this Bishop cannot collide
	 * with any other Piece on its way to <code>newPos</code>.
	 *
	 * @see chess.Piece#isMoveLegal(chess.Square, chess.Square, chess.Board)
	 */
	@Override
	public boolean isMoveLegal(Square oldPos, Square newPos, Board board) {
		if (!super.isMoveLegal(oldPos, newPos, board)) {
			return false;
		}
		int x = Math.abs(oldPos.getX() - newPos.getX());
		int y = Math.abs(oldPos.getY() - newPos.getY());
		return (x == y && !wouldCollide(oldPos, newPos, board));
	}

	/**
	 * Checks whether this Bishop would collide with any other Pieces through the
	 * proposed move. All Squares in the diagonal path between <code>oldPos</code>
	 * and <code>newPos</code> must not contain any other Piece.
	 *
	 * @see chess.Piece#wouldCollide(chess.Square, chess.Square, chess.Board)
	 */
	@Override
	public boolean wouldCollide(Square oldPos, Square newPos, Board board) {
		if (super.wouldCollide(oldPos, newPos, board)) {
			return true;
		}
		int xFactor = newPos.getX() < oldPos.getX() ? -1 : 1;
		int yFactor = newPos.getY() < oldPos.getY() ? -1 : 1;
		int y = oldPos.getY() + yFactor;
		for (int x = oldPos.getX() + xFactor; x != newPos.getX(); x += xFactor) {
			if (!board.getSquare(x, y).isEmpty()) {
				return true;
			}
			y += yFactor;
		}
		return false;
	}
}
