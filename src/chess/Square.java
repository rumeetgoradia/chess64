package chess;

/**
 * Representation of a square on a board in chess.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */

public class Square {
	/**
	 * the x (horizontal) position of the Square on the Board.
	 */
	private int x;

	/**
	 * the y (vertical) position of the Square on the Board.
	 */
	private int y;

	/**
	 * the Piece that currently is in the Square
	 */
	private Piece piece;

	/**
	 * Initializes a Square with given coordinates and Piece.
	 *
	 * @param row
	 *            the x (horizontal) position of this Square
	 * @param col
	 *            the y (vertical) position of this Square
	 * @param piece
	 *            the Piece that is to be placed in this Square
	 */
	public Square(int row, int col, Piece piece) {
		this.x = col;
		this.y = row;
		this.piece = piece;
	}

	/**
	 * Initializes a Square with given coordinates and no Piece (<code>null</code>).
	 *
	 * @param col
	 *            the x (horizontal) position of this Square
	 * @param row
	 *            the y (vertical) position of this Square
	 */
	public Square(int col, int row) {
		this(col, row, null);
	}

	/**
	 * Gets the x (horizontal) position of this Square.
	 *
	 * @return the x position of this Square
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * Gets the y (vertical) position of this Square.
	 *
	 * @return the y position of this Square
	 */
	public int getY() {
		return this.y;
	}

	/**
	 * Gets the Piece that is on this Square.
	 *
	 * @return the piece that is on the Square
	 */
	public Piece getPiece() {
		return this.piece;
	}

	/**
	 * Sets a given Piece onto a Square.
	 *
	 * @param piece
	 *            the piece that is meant to be placed on the Square
	 */
	public void setPiece(Piece piece) {
		this.piece = piece;
	}

	/**
	 * Determines whether a Square has a Piece on it.
	 *
	 * @return whether a Square has a Piece on it
	 */
	public boolean isEmpty() {
		return this.getPiece() == null;
	}

	/**
	 * Prints the Piece on this Square or the correct filling for if this Square is
	 * empty.
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (this.isEmpty()) {
			return (this.x % 2 == 0 ^ this.y % 2 == 0 ? "##" : "  ");
		} else {
			return this.piece.toString();
		}
	}
}
