package chess;

/**
 * Representation of a king in chess.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */
public class King extends Piece {

	/**
	 * Specifies whether this King is in check or not.
	 */
	private boolean checked;

	/**
	 * Specifies if this King is attempting to castle, and in what direction. 0
	 * represents no castling attempt, 1 represents a king-side castling attempt,
	 * and 2 represents a queen-side castling attempt.
	 */
	private int isCastling;

	/**
	 * Initializes a King chess Piece using type 'K'. This King cannot be in check
	 * or be attempting to castle upon initialization.
	 *
	 * @param team
	 *            the team to which this King belongs
	 */
	public King(char team) {
		super(team, 'K');
		this.checked = false;
		this.isCastling = 0;
	}

	/**
	 * Checks the legality of the proposed move for this King. A check for castling
	 * and its legality occurs first, then a normal check of movement of only one
	 * Square in any direction occurs. A separate collision check is not required
	 * here, as there are no Squares in between <code>oldPos</code> and
	 * <code>newPos</code>; therefore, the superclass's version of
	 * <code>wouldCollide</code> suffices.
	 *
	 * @see chess.Piece#isMoveLegal(chess.Square, chess.Square, chess.Board)
	 */
	@Override
	public boolean isMoveLegal(Square oldPos, Square newPos, Board board) {
		/* Check for castling */
		if (Board.isInBounds(newPos) && Math.abs(oldPos.getX() - newPos.getX()) == 2) {
			if (oldPos.getY() != newPos.getY()) {
				return false;
			}
			if (this.hasMoved() || this.checked) {
				return false;
			}
			Piece potentialRook;
			if (newPos.getX() == 6) {
				potentialRook = board.getSquare(7, newPos.getY()).getPiece();
				if (potentialRook.getType() == 'R' && !potentialRook.hasMoved()) {
					for (int i = oldPos.getX() + 1; i < 7; ++i) {
						/* Check if in-between squares aren't empty or would be in check */
						if (!board.getSquare(i, oldPos.getY()).isEmpty()
								|| Move.wouldBeInCheck(oldPos, board.getSquare(i, oldPos.getY()), board)) {
							return false;
						}
					}
				} else {
					return false;
				}
				this.isCastling = 1;
				return true;
			} else if (newPos.getX() == 2) {
				potentialRook = board.getSquare(0, newPos.getY()).getPiece();
				if (potentialRook.getType() == 'R' && !potentialRook.hasMoved()) {
					for (int i = oldPos.getX() - 1; i > 1; --i) {
						/* Check if in-between squares aren't empty or would be in check */
						if (!board.getSquare(i, oldPos.getY()).isEmpty()
								|| Move.wouldBeInCheck(oldPos, board.getSquare(i, oldPos.getY()), board)) {
							return false;
						}
					}
				} else {
					return false;
				}
				this.isCastling = 2;
				return true;
			} else {
				return false;
			}
		}
		if (!super.isMoveLegal(oldPos, newPos, board)) {
			return false;
		}
		int x = Math.abs(oldPos.getX() - newPos.getX());
		int y = Math.abs(oldPos.getY() - newPos.getY());
		return (x <= 1 && y <= 1);
	}

	/**
	 * Returns whether this King is in check or not.
	 *
	 * @return whether this King is in check or not
	 */
	public boolean isChecked() {
		return this.checked;
	}

	/**
	 * Sets whether this King is in check or not.
	 *
	 * @param checked
	 *            whether this King is in check or not
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	/**
	 * Returns whether this King is attempting to castle or not, and if so in what
	 * direction.
	 *
	 * @return whether this King is attempting to castle or not, and if so in what
	 *         direction
	 */
	public int isCastling() {
		return this.isCastling;
	}

	/**
	 * Sets whether this King is attempting to castle or not, and if so in what
	 * direction.
	 *
	 * @param isCastling
	 *            whether this King is attempting to castle or not, and if so in
	 *            what direction
	 */
	public void setCastling(int isCastling) {
		this.isCastling = isCastling;
	}

}
