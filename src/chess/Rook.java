package chess;

/**
 * Representation of a rook in chess.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */
public class Rook extends Piece {

	/**
	 * Initializes a Rook chess Piece using type 'R'.
	 *
	 * @param team
	 *            the team to which this Rook belongs
	 */
	public Rook(char team) {
		super(team, 'R');
	}

	/**
	 * Checks the legality of the proposed move for this Rook. The move must be only
	 * horizontal or vertical, and this Rook cannot collide with any other Pieces on
	 * the way to <code>newPos</code>.
	 *
	 * @see chess.Piece#isMoveLegal(chess.Square, chess.Square, chess.Board)
	 */
	@Override
	public boolean isMoveLegal(Square oldPos, Square newPos, Board board) {
		if (!super.isMoveLegal(oldPos, newPos, board)) {
			return false;
		}
		int x = Math.abs(oldPos.getX() - newPos.getX());
		int y = Math.abs(oldPos.getY() - newPos.getY());
		return ((x > 0 ^ y > 0) && !wouldCollide(oldPos, newPos, board));
	}

	/**
	 * Checks if the Rook would collide with any other Pieces through the proposed
	 * move. If the Player is attempting to move this Rook horizontally, then only a
	 * horizontal check from <code>oldPos</code> to <code>newPos</code> needs to be
	 * done. If the Player is attempting to move this Rook vertically, then only a
	 * vertical check from <code>oldPos</code> to <code>newPos</code> needs to be
	 * done.
	 *
	 * @see chess.Piece#wouldCollide(chess.Square, chess.Square, chess.Board)
	 */
	@Override
	public boolean wouldCollide(Square oldPos, Square newPos, Board board) {
		if (super.wouldCollide(oldPos, newPos, board)) {
			return true;
		}
		int x = Math.abs(oldPos.getX() - newPos.getX());
		int y = Math.abs(oldPos.getY() - newPos.getY());
		if (x > 0 ^ y > 0) {
			int min;
			int max;
			if (x > 0) {
				min = Math.min(oldPos.getX(), newPos.getX());
				max = Math.max(oldPos.getX(), newPos.getX());
				for (int i = min + 1; i < max; ++i) {
					if (!board.getSquare(i, oldPos.getY()).isEmpty()) {
						return true;
					}
				}
			} else {
				min = Math.min(oldPos.getY(), newPos.getY());
				max = Math.max(oldPos.getY(), newPos.getY());
				for (int i = min + 1; i < max; ++i) {
					if (!board.getSquare(oldPos.getX(), i).isEmpty()) {
						return true;
					}
				}
			}
		}
		return false;
	}

}
