package chess;

/**
 * Representation of a board in chess.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */
public class Board {

	/**
	 * Two-dimensional array of Squares that represents the board.
	 */
	private Square[][] board = new Square[8][8];

	/**
	 * Creates this Board by placing all Pieces in their starting positions.
	 */
	public Board() {
		this.initializeBoard();
	}

	/**
	 * Places all Pieces in their starting positions on this Board. First, the top
	 * and bottom rows are filled with Kings, Queens, Bishops, Knights, and Rooks of
	 * the respective teams. Then, the second and second to last rows are filled
	 * with Pawns of their respective teams. Finally, all remaining spots are filled
	 * with empty Squares.
	 */
	private void initializeBoard() {
		// First and last rows
		for (int r = 0; r < this.board.length; r += this.board.length - 1) {
			for (int c = 0; c < this.board[r].length; ++c) {
				Piece piece;
				switch (c) {
				case 0:
					piece = new Rook(r == 0 ? 'b' : 'w');
					break;
				case 1:
					piece = new Knight(r == 0 ? 'b' : 'w');
					break;
				case 2:
					piece = new Bishop(r == 0 ? 'b' : 'w');
					break;
				case 3:
					piece = new Queen(r == 0 ? 'b' : 'w');
					break;
				case 4:
					piece = new King(r == 0 ? 'b' : 'w');
					break;
				case 5:
					piece = new Bishop(r == 0 ? 'b' : 'w');
					break;
				case 6:
					piece = new Knight(r == 0 ? 'b' : 'w');
					break;
				case 7:
					piece = new Rook(r == 0 ? 'b' : 'w');
					break;
				default:
					piece = null;
				}
				board[r][c] = new Square(r, c, piece);
			}
		}
		// Pawn rows
		for (int r = 1; r < this.board.length; r += this.board.length - 3) {
			for (int c = 0; c < this.board[r].length; ++c) {
				board[r][c] = new Square(r, c, new Pawn(r == 1 ? 'b' : 'w'));
			}
		}
		// Empty rows
		for (int r = 2; r < this.board.length - 2; ++r) {
			for (int c = 0; c < this.board[r].length; ++c) {
				board[r][c] = new Square(r, c);
			}
		}

	}

	/**
	 * Prints out this labeled Board with the Pieces in their respective positions
	 * at the time this method is called.
	 *
	 * @param first
	 *            if this is the first move of the game
	 */
	public void printBoard(boolean first) {
		if (!first) {
			System.out.print("\n");
		}
		// Print main board
		for (int r = 0; r < this.board.length; ++r) {
			for (int c = 0; c < this.board[r].length; ++c) {
				System.out.print(this.board[r][c]);
				System.out.print(" ");
			}
			System.out.print(8 - r + "\n");
		}
		// Print column labels
		for (char col = 'a'; col <= 'h'; ++col) {
			if (col == 'a') {
				System.out.print(" " + col);
			} else {
				System.out.print("  " + col);
			}
		}
		System.out.print("\n\n");
	}

	/**
	 * Determines whether a given pair of x and y coordinates fall in the boundaries
	 * of this Board.
	 *
	 * @param x
	 *            the x (horizontal) position of the Square in question
	 * @param y
	 *            the y (vertical) position of the Square in question
	 * @return whether the given coordinates fall in-bounds
	 */
	public static boolean isInBounds(int x, int y) {
		return !(x < 0 || x > 7 || y < 0 || y > 7);
	}

	/**
	 * Determines whether a given Square falls within the Board.
	 *
	 * @param sq
	 *            the Square in question
	 * @return whether the Square is on the Board
	 */
	public static boolean isInBounds(Square sq) {
		return sq != null && isInBounds(sq.getX(), sq.getY());
	}

	/**
	 * Finds a Square on this Board given a set of coordinates.
	 *
	 * @param col
	 *            the coordinate of the column of the Square in question
	 * @param row
	 *            the coordinate of the row of the Square in question
	 * @return the Square that is trying to be found
	 */
	public Square getSquare(int col, int row) {
		if (!isInBounds(col, row)) {
			return null;
		} else {
			return this.board[row][col];
		}
	}

}
