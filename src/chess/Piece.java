package chess;

/**
 * Abstraction for all types of chess pieces.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */
public abstract class Piece {

	/**
	 * Specifies the team of this Piece ('w' for white, 'b' for black).
	 */
	private char team;

	/**
	 * Specifies the type of this Piece ('K' for king, 'Q' for queen, 'B' for
	 * bishop, 'N' for knight, 'R' for rook, 'p' for pawn).
	 */
	private char type;

	/**
	 * Indicates whether the Piece has been moved at all.
	 */
	private boolean moved;

	/**
	 * Sole constructor for a chess Piece. <code>moved</code> is immediately set to
	 * false for a newly created Piece.
	 *
	 * @param team
	 *            the team of this Piece
	 * @param type
	 *            the type of this Piece
	 */
	public Piece(char team, char type) {
		this.team = team;
		this.type = type;
		this.moved = false;
	}

	/**
	 * Returns the team of this Piece.
	 *
	 * @return the team of this Piece
	 */
	public char getTeam() {
		return team;
	}

	/**
	 * Returns the type of this Piece.
	 *
	 * @return the type of this Piece
	 */
	public char getType() {
		return type;
	}

	/**
	 * Sets the type of this Piece.
	 *
	 * @param type
	 *            the new type for this Piece
	 */
	public void setType(char type) {
		this.type = type;
	}

	/**
	 * Returns whether this Piece has been moved successfully or not.
	 *
	 * @return whether this Piece has been moved successfully or not
	 */
	public boolean hasMoved() {
		return this.moved;
	}

	/**
	 * Sets whether or not this Piece has been moved successfully.
	 *
	 * @param moved
	 *            whether this Piece was successfully moved
	 */
	public void setMoved(boolean moved) {
		this.moved = moved;
	}

	/**
	 * Checks whether the proposed move matches the specifications for legal moves
	 * for this Piece. Regardless of the type of Piece, the proposed new position of
	 * this Piece must be in bounds and must not contain collide with another Piece
	 * on its way from <code>oldPos</code> to <code>newPos</code>.
	 *
	 * @param oldPos
	 *            this Piece's current position
	 * @param newPos
	 *            this Piece's proposed new position
	 * @param board
	 *            the Board on which this Piece is being played
	 * @return whether the proposed move from <code>oldPos</code> to
	 *         <code>newPos</code> is legal
	 */
	public boolean isMoveLegal(Square oldPos, Square newPos, Board board) {
		if (Board.isInBounds(newPos) && newPos.isEmpty()) {
			return Board.isInBounds(newPos);
		}
		return (Board.isInBounds(newPos) && !wouldCollide(oldPos, newPos, board));
	}

	/**
	 * Checks whether the proposed move would cause this Piece to collide with any
	 * other Piece. Regardless of the type of Piece, the proposed new position of
	 * this Piece cannot contain another Piece of the same team.
	 *
	 * @param oldPos
	 *            this Piece's current position
	 * @param newPos
	 *            this Piece's proposed new position
	 * @param board
	 *            the Board on which this Piece is being played
	 * @return whether the proposed move from <code>oldPos</code> to
	 *         <code>newPos</code> would cause a collision
	 */
	public boolean wouldCollide(Square oldPos, Square newPos, Board board) {
		return ((!newPos.isEmpty()) && newPos.getPiece().getTeam() == this.team);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "" + this.team + this.type;
	}
}
