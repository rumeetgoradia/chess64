package chess;

/**
 * Representation of a queen in chess.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */
public class Queen extends Piece {

	/**
	 * Initializes a Queen chess Piece using type 'Q'.
	 *
	 * @param team
	 *            the team to which this Queen belongs
	 */
	public Queen(char team) {
		super(team, 'Q');
	}

	/**
	 * Checks the legality of the proposed move for this Queen. This Piece can move
	 * in a Rook-like manner or Bishop-like manner, so this legality check is
	 * essentially a combination of those two Piece types' <code>isMoveLegal</code>
	 * methods.
	 *
	 * @see chess.Piece#isMoveLegal(chess.Square, chess.Square, chess.Board)
	 */
	@Override
	public boolean isMoveLegal(Square oldPos, Square newPos, Board board) {
		if (!super.isMoveLegal(oldPos, newPos, board)) {
			return false;
		}
		int x = Math.abs(oldPos.getX() - newPos.getX());
		int y = Math.abs(oldPos.getY() - newPos.getY());
		return (((x > 0 ^ y > 0) || x == y) && !wouldCollide(oldPos, newPos, board));
	}

	/**
	 * Checks whether this Queen would collide with any other Piece through the
	 * proposed move. This Piece can move in a Rook-like manner or Bishop-like
	 * manner, so this collision check is essentially a combination of those two
	 * Piece types' <code>wouldCollide</code> methods.
	 *
	 * @see chess.Piece#wouldCollide(chess.Square, chess.Square, chess.Board)
	 */
	@Override
	public boolean wouldCollide(Square oldPos, Square newPos, Board board) {
		if (super.wouldCollide(oldPos, newPos, board)) {
			return true;
		}
		int x = Math.abs(oldPos.getX() - newPos.getX());
		int y = Math.abs(oldPos.getY() - newPos.getY());
		if (x > 0 ^ y > 0) {
			int min;
			int max;
			if (x > 0) {
				min = Math.min(oldPos.getX(), newPos.getX());
				max = Math.max(oldPos.getX(), newPos.getX());
				for (int i = min + 1; i < max; ++i) {
					if (!board.getSquare(i, oldPos.getY()).isEmpty()) {
						return true;
					}
				}
			} else {
				min = Math.min(oldPos.getY(), newPos.getY());
				max = Math.max(oldPos.getY(), newPos.getY());
				for (int i = min + 1; i < max; ++i) {
					if (!board.getSquare(oldPos.getX(), i).isEmpty()) {
						return true;
					}
				}
			}
		} else if (x == y) {
			int minX = Math.min(oldPos.getX(), newPos.getX());
			int y2 = Math.min(oldPos.getY(), newPos.getY()) + 1;
			int maxX = Math.max(oldPos.getX(), newPos.getX());
			for (int i = minX + 1; i < maxX; ++i) {
				if (!board.getSquare(i, y2).isEmpty()) {
					return true;
				}
				++y2;
			}
		}
		return false;
	}

}
