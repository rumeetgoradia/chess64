package chess;

import java.util.Scanner;

/**
 * Contains the main game loop for the Chess program. This class takes in user
 * input, makes the appropriate move, and reflects the respective changes in the
 * displayed board.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */
public class Chess {

	/**
	 * Maintains a game loop for the Chess program. The moving player is determined
	 * by <code>isWhiteTurn</code>, and the game continues running as long as
	 * <code>gameOngoing</code> is <code>true</code>. The game board's current state
	 * is displayed at the beginning of every turn, and the current player's move is
	 * checked for legality and enacted through the <code>makeMove</code> of the
	 * <code>Move</code> class. Checkmate and check are checked for after every
	 * legal move that is not a resignation or acceptance of a draw request.
	 *
	 * @param args
	 *            required but unused parameter for this method
	 */
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		Board board = new Board();
		Player white = new Player('w');
		Player black = new Player('b');

		boolean isWhiteTurn = true;
		Player currentPlayer = white;
		Player bystandingPlayer = black;
		boolean gameOngoing = true;
		int moveCounter = 0;
		boolean drawRequested = false;

		final int ILLEGAL = -1;
		final int STANDARD = 0;
		final int DRAW_REQUEST = 1;
		final int DRAW_ACCEPT = 2;
		final int RESIGN = 3;

		do {
			currentPlayer = isWhiteTurn ? white : black;
			bystandingPlayer = isWhiteTurn ? black : white;
			board.printBoard(moveCounter == 0);
			int moveResponse = 0;
			String move = "";
			do {
				if (moveResponse == ILLEGAL) {
					System.out.println("Illegal move, try again");
				}
				if (isWhiteTurn) {
					System.out.print("White's turn: ");
				} else {
					System.out.print("Black's turn: ");
				}
				move = input.nextLine().trim();
				moveResponse = Move.makeMove(move, currentPlayer, board, drawRequested);
			} while (moveResponse == ILLEGAL);
			if (moveResponse == STANDARD || moveResponse == DRAW_REQUEST) {
				if (Move.isInCheckmate(board.getSquare(bystandingPlayer.getKingX(), bystandingPlayer.getKingY()),
						board)) {
					gameOngoing = false;
					System.out.println("Checkmate");
					if (isWhiteTurn) {
						System.out.println("White wins");
					} else {
						System.out.println("Black wins");
					}
				} else if (Move.isInCheck(board.getSquare(bystandingPlayer.getKingX(), bystandingPlayer.getKingY()),
						board)) {
					System.out.println("Check");
					((King) board.getSquare(bystandingPlayer.getKingX(), bystandingPlayer.getKingY()).getPiece())
							.setChecked(true);
				} else {
					((King) board.getSquare(bystandingPlayer.getKingX(), bystandingPlayer.getKingY()).getPiece())
							.setChecked(false);
				}
			} else if (moveResponse == RESIGN) {
				gameOngoing = false;
				if (isWhiteTurn) {
					System.out.println("Black wins");
				} else {
					System.out.println("White wins");
				}
			} else if (moveResponse == DRAW_ACCEPT) {
				gameOngoing = false;
				System.out.println("draw");
			}
			drawRequested = moveResponse == DRAW_REQUEST;
			++moveCounter;
			isWhiteTurn = !isWhiteTurn;
		} while (gameOngoing);

		input.close();
	}

}
