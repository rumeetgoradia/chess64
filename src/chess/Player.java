package chess;

/**
 * Representation of a player in a game of chess.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */

public class Player {
	/**
	 * The team of this Player.
	 */
	private char team;
	/**
	 * The x position of this Player's King.
	 */
	private int kingX;

	/**
	 * The y position of this Player's King.
	 */
	private int kingY;

	/**
	 * Initializes a Player with a given team.
	 *
	 * @param team
	 *            the team to which this Player is assigned
	 */
	public Player(char team) {
		this.team = team;
		this.kingX = 4;
		this.kingY = team == 'w' ? 7 : 0;
	}

	/**
	 * Gets the team of this Player.
	 *
	 * @return the team of this player
	 */
	public char getTeam() {
		return this.team;
	}

	/**
	 * Gets the x (horizontal) position of this Player's King.
	 *
	 * @return the value of the x position of this Player's King
	 */
	public int getKingX() {
		return this.kingX;
	}

	/**
	 * Gets the y (vertical) position of this Player's King.
	 *
	 * @return the value of the y position of this Player's King
	 */
	public int getKingY() {
		return this.kingY;
	}

	/**
	 * Sets the x and y positions of the King of this Player.
	 *
	 * @param kingX
	 *            the x position that the King is being set to
	 * @param kingY
	 *            the y position that the King is being set to
	 */
	public void setKingPos(int kingX, int kingY) {
		this.kingX = kingX;
		this.kingY = kingY;
	}
}
