package chess;

/**
 * Representation of a pawn in chess.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */
public class Pawn extends Piece {

	/**
	 * Specifies whether this Pawn is attempting an en passant.
	 */
	private boolean isEnPassanting;

	/**
	 * Specifies whether this Pawn can be attacked through en passant.
	 */
	private boolean isEnPassantable;

	/**
	 * Initializes a Pawn chess Piece using type 'p'. This Pawn cannot attack or be
	 * attacked through en passant upon initialization.
	 *
	 * @param team
	 *            the team to which this Pawn belongs
	 */
	public Pawn(char team) {
		super(team, 'p');
		isEnPassanting = false;
		isEnPassantable = false;
	}

	/**
	 * Checks the legality of the proposed move for this Pawn. A check for an attack
	 * through en passant occurs first, and then a check for a regular Pawn attack
	 * occurs. Besides attacking, this Pawn can only traverse 1 Square in the
	 * forward direction (determined by its team), or 2 Squares if it has not yet
	 * moved. If this Pawn is attempting to move 2 Squares, then a collision check
	 * must occur.
	 *
	 * @see chess.Piece#isMoveLegal(chess.Square, chess.Square, chess.Board)
	 */
	@Override
	public boolean isMoveLegal(Square oldPos, Square newPos, Board board) {
		this.setEnPassanting(false);
		this.setEnPassantable(false);
		if (!super.isMoveLegal(oldPos, newPos, board)) {
			return false;
		}
		if (this.getTeam() == 'w' && newPos.getY() >= oldPos.getY()) {
			return false;
		}
		if (this.getTeam() == 'b' && newPos.getY() <= oldPos.getY()) {
			return false;
		}
		int x = Math.abs(oldPos.getX() - newPos.getX());
		int y = Math.abs(oldPos.getY() - newPos.getY());
		if (x == 1 && y == 1 && newPos.isEmpty()) {
			if (this.getTeam() == 'w') {
				Piece piece = board.getSquare(newPos.getX(), newPos.getY() + 1).getPiece();
				if (piece != null && oldPos.getY() == 3 && piece.getType() == 'p' && piece.getTeam() == 'b'
						&& ((Pawn) piece).isEnPassantable()) {
					this.setEnPassanting(true);
					return true;
				}
			} else if (this.getTeam() == 'b') {
				Piece piece = board.getSquare(newPos.getX(), newPos.getY() - 1).getPiece();
				if (piece != null && oldPos.getY() == 4 && piece.getType() == 'p' && piece.getTeam() == 'w'
						&& ((Pawn) piece).isEnPassantable()) {
					this.setEnPassanting(true);
					return true;
				}
			} else {
				return false;
			}
		} else if (x == 1 && y == 1 && !newPos.isEmpty()) {
			return true;
		} else if (!this.hasMoved()) {
			boolean wouldCollide = this.wouldCollide(oldPos, newPos, board);
			this.setEnPassantable(y == 2 && x == 0 && !wouldCollide);
			return (y <= 2 && y > 0 && x == 0 && !wouldCollide);
		}
		return (y == 1 && x == 0);
	}

	/**
	 * Checks whether this Pawn would collide with any other Pieces through the
	 * proposed move. This check only needs to occur if this Pawn legally moved 2
	 * Squares forward.
	 *
	 * @see chess.Piece#wouldCollide(chess.Square, chess.Square, chess.Board)
	 */
	@Override
	public boolean wouldCollide(Square oldPos, Square newPos, Board board) {
		if (super.wouldCollide(oldPos, newPos, board)) {
			return true;
		}
		if (Math.abs(oldPos.getY() - newPos.getY()) == 2) {
			int yFactor = this.getTeam() == 'w' ? -1 : 1;
			return !(board.getSquare(oldPos.getX(), oldPos.getY() + yFactor).isEmpty());
		}
		return false;
	}

	/**
	 * Returns whether this Pawn is attempting an en passant.
	 *
	 * @return whether this Pawn is attempting to attack through en passant
	 */
	public boolean isEnPassanting() {
		return this.isEnPassanting;
	}

	/**
	 * Sets whether this Pawn is attempting an en passant.
	 *
	 * @param enPassanting
	 *            whether this Pawn is attempting to attack through en passant
	 */
	public void setEnPassanting(boolean enPassanting) {
		this.isEnPassanting = enPassanting;
	}

	/**
	 * Returns whether this Pawn can be attacked through en passant.
	 *
	 * @return whether this Pawn can be attacked through en passant
	 */
	public boolean isEnPassantable() {
		return this.isEnPassantable;
	}

	/**
	 * Sets whether this Pawn can be attacked through en passant.
	 *
	 * @param enPassantable
	 *            whether this Pawn can be attacked through en passant
	 */
	public void setEnPassantable(boolean enPassantable) {
		this.isEnPassantable = enPassantable;
	}

}
