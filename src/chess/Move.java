package chess;

import java.util.ArrayList;

/**
 * Conditions for the validity of a move outside its type restriction.
 *
 * @author Rumeet Goradia (177008120, rug5) - Section 4
 * @author Nihar Prabhala (179005552, np642) - Section 2
 *
 */
public class Move {

	/**
	 * The Square that contains the Pawn that can be attacked through en passant. At
	 * any given point, there can only be a maximum of one such Pawn.
	 */
	private static Square enPassantablePawnSquare = null;

	/**
	 * Checks whether a King is in check after opposing Player makes a move. This
	 * method checks whether a King is in check from the different types of pieces
	 * that an opposing Player could have based on the legal moves that each of the
	 * pieces can make.
	 *
	 * @param king
	 *            the Square on which the King we are checking currently resides
	 * @param board
	 *            the Board that is being played on
	 * @return whether the King is in check
	 */
	public static boolean isInCheck(Square king, Board board) {
		int xPos = king.getX();
		int yPos = king.getY();
		char team = king.getPiece().getTeam();
		if (kingCheck(xPos, yPos, team, board) || pawnCheck(xPos, yPos, team, board)
				|| knightCheck(xPos, yPos, team, board) || rookCheck(xPos, yPos, team, board)
				|| bishopCheck(xPos, yPos, team, board)) {
			return true;
		}
		return false;
	}

	/**
	 * Checks whether King piece would be in check when Player of same team attempts
	 * a move. If the King would be in check due to this move, it is not allowed and
	 * the pieces are put back to their Squares and the move is never made.
	 *
	 * @param kingOriginal
	 *            the current Square on which the King is
	 * @param kingNew
	 *            the Square on which the King could be given a certain move
	 * @param board
	 *            the Board that is being played on
	 * @return whether a King would be in check in new position based on a legal
	 *         move
	 */
	public static boolean wouldBeInCheck(Square kingOriginal, Square kingNew, Board board) {
		if (!kingNew.isEmpty() || !(kingOriginal.getPiece().getType() == 'K')) {
			return false;
		}
		// dummy move
		King king = (King) kingOriginal.getPiece();
		kingOriginal.setPiece(null);
		kingNew.setPiece(king);
		boolean wouldBeInCheck = isInCheck(kingNew, board);
		// undo dummy move
		kingOriginal.setPiece(king);
		kingNew.setPiece(null);
		return wouldBeInCheck;
	}

	/**
	 * Checks whether a King piece is in checkmate. If a King piece is in check and
	 * all other Squares that it can move to are also Squares that keep the King in
	 * check, the King is deemed to be in checkmate.
	 *
	 * @param kingPos
	 *            the Square that the King is currently on
	 * @param board
	 *            the Board on which the game is being played
	 * @return whether the King is in checkmate
	 */
	public static boolean isInCheckmate(Square kingPos, Board board) {
		if (kingPos.isEmpty() || !(kingPos.getPiece().getType() == 'K')) {
			return false;
		}
		King king = (King) kingPos.getPiece();
		int x = kingPos.getX();
		int y = kingPos.getY();
		// If the king can move to a spot and wouldn't be in check at that spot, then it
		// is not in checkmate
		if ((king.isMoveLegal(kingPos, board.getSquare(x - 1, y - 1), board) // UP LEFT
				&& !wouldBeInCheck(kingPos, board.getSquare(x - 1, y - 1), board))
				|| (king.isMoveLegal(kingPos, board.getSquare(x, y - 1), board)
						&& !wouldBeInCheck(kingPos, board.getSquare(x, y - 1), board))
				|| (king.isMoveLegal(kingPos, board.getSquare(x + 1, y - 1), board)
						&& !wouldBeInCheck(kingPos, board.getSquare(x + 1, y - 1), board))
				|| (king.isMoveLegal(kingPos, board.getSquare(x - 1, y), board)
						&& !wouldBeInCheck(kingPos, board.getSquare(x - 1, y), board))
				|| (king.isMoveLegal(kingPos, board.getSquare(x + 1, y), board)
						&& !wouldBeInCheck(kingPos, board.getSquare(x + 1, y), board))
				|| (king.isMoveLegal(kingPos, board.getSquare(x - 1, y + 1), board)
						&& !wouldBeInCheck(kingPos, board.getSquare(x - 1, y + 1), board))
				|| (king.isMoveLegal(kingPos, board.getSquare(x, y + 1), board)
						&& !wouldBeInCheck(kingPos, board.getSquare(x, y + 1), board))
				|| (king.isMoveLegal(kingPos, board.getSquare(x + 1, y + 1), board)
						&& !wouldBeInCheck(kingPos, board.getSquare(x + 1, y + 1), board))) { // DOWN RIGHT
			return false;
		}
		// Else, all depends on whether it is currently in check
		return isInCheck(kingPos, board);
	}

	/**
	 * Determines whether a King is in check by the King of the opposing team. If
	 * there is an opposing King in one of the adjacent Squares of the Square on
	 * which the King is currently presiding, the King is in check.
	 *
	 * @param kingXPos
	 *            the King in question's horizontal position
	 * @param kingYPos
	 *            the King in question's vertical position
	 * @param team
	 *            the team to which the King belongs
	 * @param board
	 *            the Board that is being played on
	 * @return whether King is in check by the other King
	 */
	private static boolean kingCheck(int kingXPos, int kingYPos, char team, Board board) {
		int[][] positions = { { kingXPos - 1, kingYPos - 1 }, { kingXPos, kingYPos - 1 },
				{ kingXPos + 1, kingYPos - 1 }, { kingXPos - 1, kingYPos }, { kingXPos + 1, kingYPos },
				{ kingXPos - 1, kingYPos + 1 }, { kingXPos, kingYPos + 1 }, { kingXPos + 1, kingYPos + 1 } };
		for (int i = 0; i < positions.length; ++i) {
			if (!Board.isInBounds(positions[i][0], positions[i][1])
					|| board.getSquare(positions[i][0], positions[i][1]).isEmpty()) {
				continue;
			}
			Piece piece = board.getSquare(positions[i][0], positions[i][1]).getPiece();
			if (piece.getType() == 'K' && piece.getTeam() != team) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Determines whether a King is in check by a Pawn of the opposing team. A Pawn
	 * can only kill another Piece by moving diagonally, so this method checks only
	 * those Squares that are in a threatening position to the King are resided on
	 * by a Pawn.
	 *
	 * @param kingXPos
	 *            the King in question's horizontal position
	 * @param kingYPos
	 *            the King in question's vertical position
	 * @param team
	 *            the team to which the King belongs
	 * @param board
	 *            the Board that is being played on
	 * @return whether the King is in check by opposing Pawn
	 */
	private static boolean pawnCheck(int kingXPos, int kingYPos, char team, Board board) {
		if (team == 'b') {
			if (kingYPos == 7) {
				return false;
			}
			if (kingXPos != 0 && kingXPos != 7) {
				Square checkS1 = board.getSquare(kingXPos + 1, kingYPos + 1);
				Square checkS2 = board.getSquare(kingXPos - 1, kingYPos + 1);
				if ((!checkS1.isEmpty() && checkS1.getPiece().getType() == 'p' && checkS1.getPiece().getTeam() == 'w')
						|| (!checkS2.isEmpty() && checkS2.getPiece().getType() == 'p'
								&& checkS2.getPiece().getTeam() == 'w')) {
					return true;
				}
			} else {
				Square checkS1 = null;
				if (kingXPos == 0) {
					checkS1 = board.getSquare(kingXPos + 1, kingYPos + 1);
				} else if (kingXPos == 7) {
					checkS1 = board.getSquare(kingXPos - 1, kingYPos + 1);
				}
				if (!checkS1.isEmpty() && checkS1.getPiece().getType() == 'p' && checkS1.getPiece().getTeam() == 'w') {
					return true;
				}
			}
		} else if (team == 'w') {
			if (kingYPos == 0) {
				return false;
			}
			if (kingXPos != 0 && kingXPos != 7) {
				Square checkS1 = board.getSquare(kingXPos + 1, kingYPos - 1);
				Square checkS2 = board.getSquare(kingXPos - 1, kingYPos - 1);
				if ((!checkS1.isEmpty() && checkS1.getPiece().getType() == 'p' && checkS1.getPiece().getTeam() == 'b')
						|| (!checkS2.isEmpty() && checkS2.getPiece().getType() == 'p'
								&& checkS2.getPiece().getTeam() == 'b')) {
					return true;
				}
			} else {
				Square checkS1 = null;
				if (kingXPos == 0) {
					checkS1 = board.getSquare(kingXPos + 1, kingYPos - 1);
				} else if (kingXPos == 7) {
					checkS1 = board.getSquare(kingXPos - 1, kingYPos - 1);
				}
				if (!checkS1.isEmpty() && checkS1.getPiece().getType() == 'p' && checkS1.getPiece().getTeam() == 'b') {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Determines whether a King is in check by a Knight of the opposing team. On
	 * any given Square on the board, a King may be put in check by a Knight from up
	 * to 8 different Squares. Each of these 8 possible Squares is checked for a
	 * Knight and if so, the King is in check by that Knight.
	 *
	 * @param kingXPos
	 *            the King in question's horizontal position
	 * @param kingYPos
	 *            the King in question's vertical position
	 * @param team
	 *            the team to which the King belongs
	 * @param board
	 *            the Board that is being played on
	 * @return whether the King is in check by opposing Knight
	 */
	private static boolean knightCheck(int kingXPos, int kingYPos, char team, Board board) {
		ArrayList<int[]> knights = new ArrayList<int[]>();
		int[] one = new int[] { kingXPos - 1, kingYPos - 2 };
		knights.add(one);
		int[] two = new int[] { kingXPos - 2, kingYPos - 1 };
		knights.add(two);
		int[] three = new int[] { kingXPos + 1, kingYPos - 2 };
		knights.add(three);
		int[] four = new int[] { kingXPos + 2, kingYPos - 1 };
		knights.add(four);
		int[] five = new int[] { kingXPos - 2, kingYPos + 1 };
		knights.add(five);
		int[] six = new int[] { kingXPos - 1, kingYPos + 2 };
		knights.add(six);
		int[] seven = new int[] { kingXPos + 1, kingYPos + 2 };
		knights.add(seven);
		int[] eight = new int[] { kingXPos + 2, kingYPos + 1 };
		knights.add(eight);

		if (team == 'b') {
			for (int[] x : knights) {
				if (Board.isInBounds(x[0], x[1])) {
					Square k = board.getSquare(x[0], x[1]);
					if (!k.isEmpty() && k.getPiece().getType() == 'K' && k.getPiece().getTeam() == 'w') {
						return true;
					}
				}
			}
		} else if (team == 'w') {
			for (int[] x : knights) {
				if (Board.isInBounds(x[0], x[1])) {
					Square k = board.getSquare(x[0], x[1]);
					if (!k.isEmpty() && k.getPiece().getType() == 'K' && k.getPiece().getTeam() == 'b') {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Determines whether a King is in check by a Rook or orthogonal movement of
	 * Queen of the opposing team. A Rook can only move vertically and horizontally.
	 * Using the King's position, the Squares that are orthogonal to this position
	 * are checked for either a Rook or Queen and whether it is all empty Squares
	 * between them. If so, the King is in check by either this Rook or Queen.
	 *
	 * @param kingXPos
	 *            the King in question's horizontal position
	 * @param kingYPos
	 *            the King in question's vertical position
	 * @param team
	 *            the team to which the King belongs
	 * @param board
	 *            the Board that is being played on
	 * @return whether the King is in check by opposing Rook or Queen
	 */
	private static boolean rookCheck(int kingXPos, int kingYPos, char team, Board board) {
		int i = kingXPos - 1;
		int j = kingXPos + 1;

		while (Board.isInBounds(i, kingYPos)) { // checks all the squares left of the king

			Piece piece = board.getSquare(i, kingYPos).getPiece();
			if (piece == null) { // empty square, skip
				i--;
				continue;
			}
			if (piece.getTeam() == team) { // piece of same team found first, break out of loop,
				break; // because it is not in check and left already initialized to false
			} else if (piece.getTeam() != team && piece.getType() != 'Q' && piece.getType() != 'R') { // any piece from
																										// other team
																										// that is
				break; // not queen or rook in between
			}
			if ((piece.getType() == 'R' || piece.getType() == 'Q') && piece.getTeam() != team) { // if the piece is a
																									// rook or queen of
				return true; // opposing team, it is in check from that direction
			}
			i--;
		}
		while (Board.isInBounds(j, kingYPos)) { // checks all squares to right of king
			Piece piece = board.getSquare(j, kingYPos).getPiece();
			if (piece == null) {
				j++;
				continue;
			}
			if (piece.getTeam() == team) {
				break;
			} else if (piece.getTeam() != team && piece.getType() != 'Q' && piece.getType() != 'R') { // any piece from
																										// other team
																										// that is
				break; // not queen or rook in between
			}
			if ((piece.getType() == 'R' || piece.getType() == 'Q') && piece.getTeam() != team) {
				return true;
			}
			j++;
		}

		int u = kingYPos - 1;
		int v = kingYPos + 1;
		while (Board.isInBounds(kingXPos, u)) {

			Piece piece = board.getSquare(kingXPos, u).getPiece();
			if (piece == null) {
				u--;
				continue;
			}
			if (piece.getTeam() == team) {
				break;
			} else if (piece.getTeam() != team && piece.getType() != 'Q' && piece.getType() != 'R') { // any piece from
																										// other team
																										// that is
				break; // not queen or rook in between
			}
			if ((piece.getType() == 'R' || piece.getType() == 'Q') && piece.getTeam() != team) {
				return true;
			}
			u--;
		}
		while (Board.isInBounds(kingXPos, v)) {

			Piece piece = board.getSquare(kingXPos, v).getPiece();
			if (piece == null) {
				v++;
				continue;
			}
			if (piece.getTeam() == team) {
				break;
			} else if (piece.getTeam() != team && piece.getType() != 'Q' && piece.getType() != 'R') { // any piece from
																										// other team
																										// that is
				break; // not queen or rook in between
			}
			if ((piece.getType() == 'R' || piece.getType() == 'Q') && piece.getTeam() != team) {
				return true;
			}
			v++;
		}

		return false;
	}

	/**
	 * Determines whether a King is in check by a Bishop or the diagonal movement of
	 * a Queen of the opposing team. A Rook can only move diagonally. Using the
	 * King's position, the Squares that are diagonal to this position are checked
	 * for either a Bishop or Queen and whether it is all empty Squares between
	 * them. If so, the King is in check by either this Bishop or Queen.
	 *
	 * @param kingXPos
	 *            the King in question's horizontal position
	 * @param kingYPos
	 *            the King in question's vertical position
	 * @param team
	 *            the team to which the King belongs
	 * @param board
	 *            the Board that is being played on
	 * @return whether the King is in check by opposing Bishop or Queen
	 */
	private static boolean bishopCheck(int kingXPos, int kingYPos, char team, Board board) {
		// gonna have to be similar logic to the rook, just gotta check the four
		// diagonal directions instead of the two vert/two horiz
		int tlx = kingXPos - 1; // tlx = top left x
		int tly = kingYPos - 1; // tly = top left y

		int trx = kingXPos + 1;
		int tr_y = kingYPos - 1;

		int blx = kingXPos - 1; // bottom left x
		int bly = kingYPos + 1; // bottom left y

		int brx = kingXPos + 1;
		int bry = kingYPos + 1;

		while (Board.isInBounds(tlx, tly)) { // top left diagonal checker
			Piece piece = board.getSquare(tlx, tly).getPiece();
			if (piece == null) {
				tlx--;
				tly--;
				continue;
			}
			if (piece.getTeam() == team) {
				break;
			} else if (piece.getTeam() != team && piece.getType() != 'Q' && piece.getType() != 'B') { // any piece from
																										// other team
																										// that is
				break; // not queen or rook in between
			}
			if (piece.getTeam() != team && (piece.getType() == 'B' || piece.getType() == 'Q')) {
				return true;
			}
			tlx--;
			tly--;
		}
		while (Board.isInBounds(trx, tr_y)) { // top right diagonal checker
			Piece piece = board.getSquare(trx, tr_y).getPiece();
			if (piece == null) {
				trx++;
				tr_y--;
				continue;
			}
			if (piece.getTeam() == team) {
				break;
			} else if (piece.getTeam() != team && piece.getType() != 'Q' && piece.getType() != 'B') { // any piece from
																										// other team
																										// that is
				break; // not queen or rook in between
			}
			if (piece.getTeam() != team && (piece.getType() == 'B' || piece.getType() == 'Q')) {
				return true;
			}
			trx++;
			tr_y--;
		}
		while (Board.isInBounds(blx, bly)) { // bottom left diagonal checker
			Piece piece = board.getSquare(blx, bly).getPiece();
			if (piece == null) {
				blx--;
				bly++;
				continue;
			}
			if (piece.getTeam() == team) {
				break;
			} else if (piece.getTeam() != team && piece.getType() != 'Q' && piece.getType() != 'B') { // any piece from
																										// other team
																										// that is
				break; // not queen or rook in between
			}
			if (piece.getTeam() != team && (piece.getType() == 'B' || piece.getType() == 'Q')) {
				return true;
			}
			blx--;
			bly++;
		}
		while (Board.isInBounds(brx, bry)) { // bottom right diagonal checker
			Piece piece = board.getSquare(brx, bry).getPiece();
			if (piece == null) {
				brx++;
				bry++;
				continue;
			}
			if (piece.getTeam() == team) {
				break;
			} else if (piece.getTeam() != team && piece.getType() != 'Q' && piece.getType() != 'B') { // any piece from
																										// other team
																										// that is
				break; // not queen or rook in between
			}
			if (piece.getTeam() != team && (piece.getType() == 'B' || piece.getType() == 'Q')) {
				return true;
			}
			brx++;
			bry++;
		}

		return false;
	}

	/**
	 * Executes a move based on Player input. This method runs checks to see whether
	 * the user input accepts a draw or resigns. If neither of these is applicable,
	 * the move will be executed as long as the move is deemed legal. If the move is
	 * illegal, the move is not executed. This method also handles the possibility
	 * of en passant with Pawns and castling with Kings.
	 *
	 * @param move
	 *            the move that the Player is trying to make
	 * @param currentPlayer
	 *            the Player who just input their move
	 * @param board
	 *            the Board that the game is being played on
	 * @param drawRequested
	 *            whether or not a draw was requested by currentPlayer
	 * @return number based on type of move made
	 */
	public static int makeMove(String move, Player currentPlayer, Board board, boolean drawRequested) {
		final int ILLEGAL = -1;
		final int STANDARD = 0;
		final int DRAW_REQUEST = 1;
		final int DRAW_ACCEPT = 2;
		final int RESIGN = 3;

		if (move.equals("resign")) {
			return RESIGN;
		} else if (move.equals("draw") && drawRequested) {
			return DRAW_ACCEPT;
		} else if (!move.equals("draw")) {
			if (!isMoveValid(move, currentPlayer, board)) {
				return ILLEGAL;
			}
			String[] moveSplit = move.split(" ");
			int[] oldCoordinates = new int[2];
			getCoordinates(moveSplit[0].trim(), oldCoordinates);
			int[] newCoordinates = new int[2];
			getCoordinates(moveSplit[1].trim(), newCoordinates);
			Piece piece = board.getSquare(oldCoordinates[0], oldCoordinates[1]).getPiece();
			if (moveSplit.length == 3 && !moveSplit[2].trim().equals("draw?") && !(piece.getType() != 'p')) {
				return ILLEGAL;
			}
			board.getSquare(oldCoordinates[0], oldCoordinates[1]).setPiece(null);
			board.getSquare(newCoordinates[0], newCoordinates[1]).setPiece(piece);
			if (piece.getType() == 'p') {
				if ((piece.getTeam() == 'b' && newCoordinates[1] == 7)
						|| (piece.getTeam() == 'w' && newCoordinates[1] == 0)) {
					if (moveSplit.length < 3) {
						board.getSquare(newCoordinates[0], newCoordinates[1]).setPiece(new Queen(piece.getTeam()));
					} else if (moveSplit.length == 3) {
						char type = moveSplit[2].trim().charAt(0);
						switch (type) {
						case 'N':
							board.getSquare(newCoordinates[0], newCoordinates[1]).setPiece(new Knight(piece.getTeam()));
							break;
						case 'B':
							board.getSquare(newCoordinates[0], newCoordinates[1]).setPiece(new Bishop(piece.getTeam()));
							break;
						case 'R':
							board.getSquare(newCoordinates[0], newCoordinates[1]).setPiece(new Rook(piece.getTeam()));
							break;
						case 'Q':
							board.getSquare(newCoordinates[0], newCoordinates[1]).setPiece(new Queen(piece.getTeam()));
							break;
						default:
							return ILLEGAL;
						}
					} else {
						return ILLEGAL;
					}
				}
			}

			if (piece.getType() == 'K') {
				currentPlayer.setKingPos(newCoordinates[0], newCoordinates[1]);
			}

			if (isInCheck(board.getSquare(currentPlayer.getKingX(), currentPlayer.getKingY()), board)) {
				board.getSquare(oldCoordinates[0], oldCoordinates[1]).setPiece(piece);
				board.getSquare(newCoordinates[0], newCoordinates[1]).setPiece(null);
				if (piece.getType() == 'K') {
					currentPlayer.setKingPos(oldCoordinates[0], oldCoordinates[1]);
				}
				return ILLEGAL;
			}

			if (enPassantablePawnSquare != null && enPassantablePawnSquare.getPiece().getType() == 'p') {
				Pawn pawn = (Pawn) enPassantablePawnSquare.getPiece();
				pawn.setEnPassantable(false);
			}

			if (piece.getType() == 'p' && ((Pawn) piece).isEnPassantable()) {
				enPassantablePawnSquare = board.getSquare(newCoordinates[0], newCoordinates[1]);
			} else {
				enPassantablePawnSquare = null;
			}

			if (piece.getType() == 'p' && ((Pawn) piece).isEnPassanting()) {
				if (piece.getTeam() == 'w') {
					board.getSquare(newCoordinates[0], newCoordinates[1] + 1).setPiece(null);
				} else if (piece.getTeam() == 'b') {
					board.getSquare(newCoordinates[0], newCoordinates[1] - 1).setPiece(null);
				}
				((Pawn) piece).setEnPassanting(false);
				enPassantablePawnSquare = null;
			}

			if (piece.getType() == 'K' && ((King) piece).isCastling() != 0) {
				King king = (King) piece;
				if (king.isCastling() == 1) {
					Rook rook = (Rook) board.getSquare(newCoordinates[0] + 1, newCoordinates[1]).getPiece();
					board.getSquare(newCoordinates[0] + 1, newCoordinates[1]).setPiece(null);
					board.getSquare(newCoordinates[0] - 1, newCoordinates[1]).setPiece(rook);
				} else if (king.isCastling() == 2) {
					Rook rook = (Rook) board.getSquare(newCoordinates[0] - 2, newCoordinates[1]).getPiece();
					board.getSquare(newCoordinates[0] - 2, newCoordinates[1]).setPiece(null);
					board.getSquare(newCoordinates[0] + 1, newCoordinates[1]).setPiece(rook);
				}
				king.setCastling(0);
			}

			piece.setMoved(true);

			if (moveSplit.length == 3 && moveSplit[2].trim().equals("draw?")) {
				return DRAW_REQUEST;
			}

			return STANDARD;

		}
		return ILLEGAL;
	}

	/**
	 * Determines whether a move can be allowed to be made (along with the legality
	 * of the move). This method checks the user has asked for piece to be moved
	 * from a Square that actually contains a piece and if the input is valid
	 * format. It calls another method to determine the legality of the move itself.
	 *
	 * @param move
	 *            the move that the Player is trying to make
	 * @param currentPlayer
	 *            the Player who just input their move
	 * @param board
	 *            the board that the game is being played on
	 * @return whether the move will be allowed
	 */
	private static boolean isMoveValid(String move, Player currentPlayer, Board board) {
		String[] moveSplit = move.split(" ");
		if (moveSplit.length < 2 || moveSplit.length > 3) {
			return false;
		}
		Square oldPos = null;
		Square newPos = null;
		for (int i = 0; i <= 1; ++i) {
			if (moveSplit[i].length() != 2) {
				return false;
			}
			if (!(Character.isLowerCase(moveSplit[i].charAt(0))) || !(Character.isDigit(moveSplit[i].charAt(1)))) {
				return false;
			}
			int[] coordinates = new int[2];
			getCoordinates(moveSplit[i].trim(), coordinates);
			Square currentSquare = board.getSquare(coordinates[0], coordinates[1]);
			if (currentSquare == null) {
				return false;
			} else if (i == 0) {
				if (currentSquare.isEmpty() || currentSquare.getPiece().getTeam() != currentPlayer.getTeam()) {
					return false;
				}

				oldPos = currentSquare;
			} else {
				newPos = currentSquare;
			}
		}
		Piece piece = oldPos.getPiece();
		return piece.isMoveLegal(oldPos, newPos, board);
	}

	/**
	 * Gets the coordinates of a Square on a standard 8x8 board based on the user
	 * input. This method converts the user input for a move into coordinates for
	 * the board matrix.
	 *
	 * @param move
	 *            the move that the Player is trying to make
	 * @param coordinates
	 *            the coordinates on the Board based on the user input
	 */
	private static void getCoordinates(String move, int[] coordinates) {
		coordinates[0] = move.charAt(0) - 'a';
		coordinates[1] = 8 - Integer.parseInt(move.substring(1));
	}

}
